""" 
1.) create your schema here:
    http://ondras.zarovi.cz/sql/demo/
        (make sure it is set to sqlite:
        - Options -> sqlite
        - then reload the page before you make tables)
        - this video will walk you through:
            - https://drive.google.com/open?id=1vtWxFLOvEO71Jy5kuKoYe32oEJ03VIXs

2.) export your schema and paste it into the schema.sql file in database/ directory
        - an example is there for you for a single table

3.) run this file via terminal

4.) check your data schema with the SQLiteStudio tool (see video above)
"""

import sqlite3

con = sqlite3.connect("database/database.db")

# your database schema must live in the schema.sql file in database/ directory
with open("database/schema.sql") as schema:
	con.executescript(schema.read())
