"""
1.) Grab game data from:

https://github.com/DeviPotato/splat2-calc/tree/master/app/data

    - easier to copy and paste if viewing file as 'raw'

2.) copy contents of square brackets (include the sq. brackets too)

3.) paste into your new file and into  https://jsonlint.com/

4.) validate json, you can use the find and replace tools in the code editor to do things more quickly

5.) once you have valid json you can overwrite the contents of your new file and save it

6.) for each table, change the details of the script below to suit your table and then run it.

7.) check that the data is inserted properly using the SQLiteStudio tool
"""

import json
import sqlite3

# --------------------------------------------------------------

# change this to point to your database (if needed)
con = sqlite3.connect("database/database.db")
# create cursor
cur = con.cursor()

# --------------------------------------------------------------

# change this to point to your json file
jsonFile = 'json/clothes.json'

# change this to the name of the table you are populating
tableName = 'clothes'

# --------------------------------------------------------------

with open(jsonFile, 'r', encoding='utf-8-sig') as f:
    jsonArray = json.load(f)

# check if needed
# print (jsonArray)

# iterate through big JSON array
for item in jsonArray:

    # assign the brand to a variable
    thisBrand = item["brand"]

    # print(thisBrand)

    # we're going to query the brands table to see if the brand exists
    # if it does we grab the brand_id for that brand so that we can use it later in our items table

    # this makes the result come back from the database as a dictionary (easy to work with)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    # see if that brand exists in the database
    cur.execute("SELECT * FROM brands WHERE brand=?",[thisBrand])
    result = cur.fetchone()
    

    # if the brand exists then assign the brand_id accordingly. Otherwise assign it to 0 for NONE
    if result:
        thisBrandID = result['brand_id']
    else:
        # 0 is for NONE in the brand part of the  JSON file
        thisBrandID = 0

    # can check stuff if needed
    # print(thisBrandID)

    # you will need to change these to retrieve values from your JSON file
    thisName = item["name"]
    thisJapName = item["localizedName"]["ja_JP"]
    thisImageURL = item["image"]
    thisMain = item["main"]
    thisStars = item["stars"]
    thisSplatnet = item["splatnet"]

    # can check stuff if needed
    # print(thisName, thisJapName)


    # Execute SQL. Notice how we've used ? as placeholders for the actual values. 
    # This is important to prevent SQL injection. The values we want to enter
    # into the database are given as a set in the exact order immediately after.
    # Be sure that the number of fields you're inserting matches the number of ? marks
    # and the number of variables. Also make sure the spelling of field names is correct.
    cur.execute("INSERT INTO "+tableName+" (name,main,image,stars,splatnet,brand_id) VALUES (?,?,?,?,?,?)",(thisName,thisMain,thisImageURL,thisStars,thisSplatnet,thisBrandID))

    # this commits the row into the table
    con.commit()

