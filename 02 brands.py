"""
This is a cheap and cheerful way to populate a table with brand names 
"""

import sqlite3
# --------------------------------------------------------------

# change this to point to your database (if needed)
con = sqlite3.connect("database/database.db")
# create cursor
cur = con.cursor()


# --------------------------------------------------------------
 
# this is just a partial list of brands ... there are a fair few more that should be added
brands = ["SquidForce","Tentatek","Rockenberg","Krak-On","Skalop","Firefin","Takoroka","Inkline","Zink","Splash Mob","Toni Kensa"]


# loop through brands
for brand in brands:

    # print(brand)
    # insert each brand into a row into the database
    cur.execute("INSERT INTO brands (brand) VALUES (?)",[brand])
    con.commit()
